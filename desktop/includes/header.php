<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
      <title> EXPRESS SI </title>
      <!-- Bootstrap -->
      <link href="./css/swiper.css" rel="stylesheet">
      <link href="./css/general.css" rel="stylesheet">
      <link href="./images/default/favicon.ico" rel="shortcut icon">
      <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]--> 
   </head>
   <body>
      <header>
         <div class="nav_color">
            <div class="container">
               <div class="row">
                  <div class="col-xs-12">
                     <div class="logo_express">  
                        <img src="./images/default/express-logo-white.png" alt="">
                     </div>
                     <nav class="nav_desk">
                        <div data-div="d1" class="naveg"> <span class="no_mar"> <a href="#">MUJER</a> </span> </div>
                        <div data-div="d2" class="naveg"> <span> <a href="#">HOMBRE</a> </span> </div>
                        <div data-div="d3" class="naveg"> <span> <a href="#">OFERTAS</a> </span> </div>
                        <div data-div="d4" class="naveg"> <span> <a href="#">TENDENCIAS</a> </span> </div>
                     </nav>
                     <nav class="nav_info">
                        <span class="info"> Envío Gratis a partir de $899 + <a href="#">Detalle</a> </span> 
                        <ul class="sbs">
                           <li class="sign"> <a href="login.php"> Entrar </a> </li>
                           <li class="bag">
                              <div class="bk_bag"> <a href="#">0</a> </div>
                           </li>
                           <li class="search">
                              <div class="bk_search"></div>
                           </li>
                        </ul>
                     </nav>
                  </div>
               </div>
               <!-- row --> 
            </div>
         </div>
         <div class="container">
            <div class="row">
               <div class="col-xs-12">
                  <div class="com d1 menu_white">
                     <div class="row rl">
                        <div class="col5">
                           <h2>Destacado</h2>
                           <ul class="options_menu">
                              <li><a href="catalog.php">Lo Nuevo</a></li>
                              <li><a href="#">Joyería Leslie Dale</a></li>
                              <li><a href="#">Joyería Deepa Gurnani</a></li>
                              <li><a href="#">Accesorios para Hogar y Viaje</a></li>
                              <li><a href="#">Outlet</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Vestidos</h2>
                           <ul class="options_menu">
                              <li><a href="#">Minivestidos</a></li>
                              <li><a href="#">Vestidos</a></li>
                              <li><a href="#">Vestidos Largos</a></li>
                              <li><a href="#">Vestidos Ajustados</a></li>
                              <li><a href="#">Vestidos Deslizamientos</a></li>
                              <li><a href="#">Vestidos Cocktail</a></li>
                              <li><a href="#">Vestidos Negros</a></li>
                     
                              <li><a href="#">Vestidos 2 piezas</a></li>
                              <li><a href="#">Boda</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Tops</h2>
                           <ul class="options_menu">
                              <li><a href="#">Express One Eleven*</a></li>
                              <li><a href="#">Bralettes and bodysuits*</a></li>
                              <li><a href="#">Camis and Tanks*</a></li>
                              <li><a href="#">Camisetas estampadas</a></li>
                              <li><a href="#">Camisetas</a></li>
                              <li><a href="#">Camisas y Blusas</a></li>
                              <li><a href="#">Tops</a></li>
                              <li><a href="#">Suéteres y Chaquetas</a></li>
                              <li><a href="#">Chaquetas y prendas de Vestir</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Inferiores</h2>
                           <ul class="options_menu">
                              <li><a href="#">Jeans</a></li>
                              <li><a href="#">Pantalones</a></li>
                              <li><a href="#">Editor Pants*</a></li>
                              <li><a href="#">Columnist Pants*</a></li>
                              <li><a href="#">Leggings</a></li>
                              <li><a href="#">Crops and Culottes</a></li>
                              <li><a href="#">Acampanados</a></li>
                              <li><a href="#">Pantalones Cortos</a></li>
                              <li><a href="#">Skirts</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Accesorios</h2>
                           <ul class="options_menu">
                              <li><a href="#">Joyería</a></li>
                              <li><a href="#">Broches y Parches</a></li>
                              <li><a href="#">Joyeria</a></li>
                              <li><a href="#">Bolsas de mano</a></li>
                              <li><a href="#">Bolsas</a></li>
                              <li><a href="#">Cinturones</a></li>
                
                              <li><a href="#">Sombreros y prendedores</a></li>
                              <li><a href="#">Medias y calcetines</a></li>
                              <li><a href="#">Fragancias</a></li>
                           </ul>
                        </div>
                     </div>
                     <div class="row rl">
                        <div class="col5">
                           <h2>Ir a trabajar</h2>
                           <ul class="options_menu">
                              <li><a href="#">Conjuntos</a></li>
                              <li><a href="#">Chaqueta sport</a></li>
                              <li><a href="#">Vestidos y Faldas</a></li>
                              <li><a href="#">Pantalones de vestir</a></li>
                              <li><a href="#">Camisas de trabajo</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Zapatos</h2>
                           <ul class="options_menu">
                              <li><a href="#">Sandalias</a></li>
                              <li><a href="#">Tacones</a></li>
                              <li><a href="#">Zapato bajo</a></li>
                              <li><a href="#">Botas</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Joyeria</h2>
                           <ul class="options_menu">
                              <li><a href="#">Collares</a></li>
                              <li><a href="#">Pulseras</a></li>
                              <li><a href="#">Anillos</a></li>
                              <li><a href="#">Aretes</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Ropa Deportiva</h2>
                           <ul class="options_menu">
                              <li><a href="#">Tops Deportivos</a></li>
                              <li><a href="#">Camisetas Deportivas</a></li>
                              <li><a href="#">Sudaderas</a></li>
                              <li><a href="#">Leggings Deportivos</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Trajes de Baño</h2>
                           <ul class="options_menu">
                              <li><a href="#">Bikini Superior</a></li>
                              <li><a href="#">Bikini Inferior</a></li>
                              <li><a href="#">Completo</a></li>
                              <li><a href="#">Vestidos de Playa</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="com d2 menu_white">
                     <div class="row rl">
                        <div class="col5">
                           <h2>Destacado</h2>
                           <ul class="options_menu">
                              <li><a href="#">Lo Nuevo</a></li>
                              <li><a href="#">Kris Bryant</a></li>
                              <li><a href="#">Marcas Destacadas</a></li>
                              <li><a href="#">Performance wear*</a></li>
                              <li><a href="#">Extra Grandes</a></li>
                              <li><a href="#">Ajustado</a></li>
                              <li><a href="#">Outlet</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Conjuntos</h2>
                           <ul class="options_menu">
                              <li><a href="#">Trajes</a></li>
                              <li><a href="#">Pantalones y Sacos</a></li>
                              <li><a href="#">Chaquetas y Chalecos</a></li>
                              <li><a href="#">Pantalones de Vestir</a></li>
                              <li><a href="#">Accesorios</a></li>
                              <li><a href="#">Como comprar un traje</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Tops</h2>
                           <ul class="options_menu">
                              <li><a href="#">Express One Eleven</a></li>
                              <li><a href="#">Camisas</a></li>
                              <li><a href="#">Camisas de Vestir y 1MX</a></li>
                              <li><a href="#">Camisas y Corbatas</a></li>
                              <li><a href="#">Camisa Informal</a></li>
                              <li><a href="#">Camisa Casual</a></li>
                              <li><a href="#">Camisetas</a></li>
                              <li><a href="#">Camisetas Estampadas</a></li>
                              <li><a href="#">Polos</a></li>
                              <li><a href="#">Suéterse y Chaquetas</a></li>
                              <li><a href="#">Sudaderas</a></li>
                              <li><a href="#">Outerwear*</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Inferiores</h2>
                           <ul class="options_menu">
                              <li><a href="#">Jeans</a></li>
                              <li><a href="#">Pantalones de algodón</a></li>
                              <li><a href="#">Joggers *</a></li>
                              <li><a href="#">Pantalones de Vestir</a></li>
                              <li><a href="#">Pantalones Cortos</a></li>
                              <li><a href="#">Trajes de Baño</a></li>
                              <li><a href="#">Ropa Interior</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Accesorios</h2>
                           <ul class="options_menu">
                              <li><a href="#">Zapatos</a></li>
                              <li><a href="#">Corbatas</a></li>
                              <li><a href="#">Ropa Interior</a></li>
                              <li><a href="#">Camista Stretch y tank *</a></li>
                              <li><a href="#">Calcetines</a></li>
                              <li><a href="#">Cinturones y Tirantes</a></li>
                              <li><a href="#">Sombreros</a></li>
                              <li><a href="#">Lentes de Sol</a></li>
                              <li><a href="#">Relojes y Joyeria</a></li>
                              <li><a href="#">Bolsas y Billeteras</a></li>
                              <li><a href="#">Colonia</a></li>
                              <li><a href="#">Tecnología y Hogar</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="com d3 menu_white">
                     <div class="row rl">
                        <div class="col5">
                           <h2>Ofertas Mujer</h2>
                           <ul class="options_menu">
                              <li><a href="#">Outlet Mujer</a></li>
                              <li><a href="#">Vestidos Oferta</a></li>
                              <li><a href="#">Tops Oferta</a></li>
                              <li><a href="#">Leggings Oferta</a></li>                              
                           </ul>
                        </div>
                        <div class="col5">
                           <h2></h2>
                           <ul class="options_menu">
                              <li><a href="#">Jeans Oferta</a></li>
                              <li><a href="#">Pantalon de Vestir Oferta</a></li>
                              <li><a href="#">Ropa Deportiva Oferta</a></li>
                              <li><a href="#">Joyeria Oferta</a></li>
                           </ul>
                        </div>
                        <div class="col5">
                           <h2>Ofertas Hombre</h2>
                           <ul class="options_menu">
                              <li><a href="#">Outlet Hombre</a></li>
                              <li><a href="#">Camisas de vestir y corbatas Oferta</a></li>
                              <li><a href="#">Polos Oferta</a></li>
                              <li><a href="#">Pantalones Cortos Oferta</a></li>                              
                           </ul>
                        </div>
                        <div class="col5">
                           <h2></h2>
                           <ul class="options_menu">
                              <li><a href="#">Suéteres Oferta</a></li>
                              <li><a href="#">Jeans Oferta</a></li>
                              <li><a href="#">Pantalón de Vestir Oferta</a></li>
                              <li><a href="#">Ropa Interior Oferta</a></li>
                           </ul>
                        </div>
                        <div class="col5"></div>
                     </div>
                  </div>
                  <div class="com d4 menu_white">
                     <div class="row rl">
                        <div class="col5">
                           <h2>Mujer</h2>
                           <ul class="options_menu">
                              <li><a href="#">Outlet Mujer</a></li>
                              <li><a href="#">Tendencia Parches</a></li>
                              <li><a href="#">Bodas</a></li>
                              <li><a href="#">Fiestas y Ocasiones</a></li>
                              <li><a href="#">Vacaciones</a></li>
                              <li><a href="#">Blanco</a></li>
                              <li><a href="#">Tendencia 90's</a></li>
                              <li><a href="#">Festival</a></li>
                              <li><a href="#">Encaje</a></li>
                              <li><a href="#">Lentejuela</a></li>
                              <li><a href="#">Must have looks *</a></li>                              
                           </ul>
                        </div>
                        <div class="col5">
                           <a href="#"><img src="./images/content/nav-flyout-w-3.jpg"></a>
                        </div>
                        <div class="col5">
                           <h2>Hombre</h2>
                           <ul class="options_menu">
                              <li><a href="#">Outlet Hombre</a></li>
                              <li><a href="#">Eventos y Ocasiones</a></li>
                              <li><a href="#">Boda</a></li>
                              <li><a href="#">Vacaciones</a></li>
                              <li><a href="#">Tendencia de colores</a></li>
                              <li><a href="#">Atajo</a></li>
                              <li><a href="#">Going out *</a></li>
                              <li><a href="#">Ir a Trabajar</a></li>
                              <li><a href="#">Camisa y Corbata</a></li>
                              <li><a href="#">Must have looks *</a></li>                      
                           </ul>
                        </div>
                        <div class="col5">
                           <a href="#"><img src="./images/content/nav-flyout-m-3.jpg"></a>
                        </div>
                        <div class="col5"></div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </header>
      <div class="container">