</div> <!-- container -->   
<footer>
   <div class="container">

   <hr>

   <div class="row">
      <div class="col-xs-4">
         <div class="group-social">
            <h4 class="title-footer">Follow Us</h4>
            <ul class="social">
               <li><a class="social-link icon-instagram" href="#" target="_blank"></a></li>
               <li><a class="social-link icon-facebook" href="#" target="_blank"></a></li>
            </ul>

            <div class="show-modal-sign">	
               <h4 class="title-footer">Newsletter</h4>
               <input type="text" class="enter_mail" placeholder="Sign up for email news">
 
            </div>

         </div>
      </div>

      <div class="col-xs-3">
         <div class="group-locator">
            <h4 class="title-footer title-locator">Buscador de Tiendas</h4>
            <p id="find-icon" class="location link-locator"><a href="#"><span class="icon-pin"></span>Encuentra una Tienda</a></p>
            <ul class="list-service links-list">
               <li><strong>Parque Delta Mall</strong></li>
               <li>Ciudad de México</li>
               <li>Get Directions</li>
            </ul>
         </div>
      </div>

      <div class="col-xs-3">
         <div class="customer">
            <h4 class="title-footer title-service">Ayuda</h4>
            <ul id="ul-service" class="list-service links-list">
 

               <li><a href="./preguntas-frecuentes.php">Preguntas Frecuentes</a></li>
               <li><a href="./devoluciones.php">Devoluciones</a></li>
               <li><a href="./politicas-de-privacidad.php">Políticas de Privacidad</a></li>
               <li><a href="./plazos-de-entrega.php">Plazos de Entrega</a></li>
               <li><a href="./terminos-y-condiciones.php">Términos y condiciones</a></li>


            </ul>

<!--
            <h6 class="title-footer">About Express</h6>
            <ul class="links-list">
               <li><a href="#">Who We Are</a></li>
            </ul>
--> 

         </div>
      </div>


      <div class="col-xs-2">
         <div class="popular">
            <h4 class="title-footer title-popular">EXPRESS</h4>
            <ul class="links-list">

               <li><a href="#">¿Qué es Express?</a></li>
               <li><a href="#">Tiendas </a></li>
               <li><a href="#">Proveedores </a></li>
               <li><a href="#">Facturación</a></li>

            </ul>
         </div>
      </div>


   </div>


   <div class="row info_links">
      <div class="col-xs-3"> 
         <img class="logo_footer" src="./images/default/logo-black.png" alt="">
      </div>

      <div class="col-xs-9">
         <ul class="sublinks-left">
            <li class="no_mar"><a href="#">Términos y condiciones</a></li>
            <li><a href="#">Política de Privacidad</a></li>
            <li>© 2016 Express.  Todos los derechos reservados</li>
         </ul>
      </div>

   </div>


</footer>
<script   src="https://code.jquery.com/jquery-1.7.js"   integrity="sha256-fBiF7IYg9AoQ0EWUjT+fe4+cT3vS/x3ftIap8n6V4+M="   crossorigin="anonymous"></script> 
<script src="./js/menu.js"></script>  
<script src="./js/swiper.js"></script>  
<script src="./js/product.js"></script>  
<script src="./js/account.js"></script>  


<script>
   
   $(".n_filter h4").click(function() { 

 var abierto = $(this).attr('class');
 
console.log(abierto); 

 if (abierto != 'undefined') {

            $(this).toggleClass('cerrado');
            $(this).next("ul.normal_filter").css("display", "none");

                             }


 if (abierto == 'cerrado') {

            $(this).toggleClass('');
            $(this).next("ul.normal_filter").css("display", "block"); 

                             }


   });

 
</script>



</body>
</html>