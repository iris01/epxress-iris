<?php include('./includes/header.php');  ?> 



<div class="row">
  
<div class="col-xs-12">   


<div class="bt_sections nf"> 
  


<h1>Términos y condiciones</h1>

<hr>

      <p>Objeto y Generalidades
         Las presentes Condiciones Generales de Uso, Condiciones de Venta y Política de Privacidad regulan el uso del sitio Web de internet www.promoda.com.mx (en adelante “el Sitio Web”), de la que MULTIBRAND OUTLET STORES, S.A.P.I. DE C.V (en adelante “Promoda”) es titular, con domicilio en la calle Ámsterdam, número 255, 4° y 5° Piso, Colonia Hipódromo Condesa, Delegación Cuauhtémoc, C.P. 06100, en México, D.F.
      </p>
      <p>Al tener acceso al Sitio y usarlo, usted conviene en sujetarse al siguiente Convenio sin limitación o calificación. Si usted no pretende sujetarse legalmente a estos términos y condiciones, no acceda ni use el sitio web www.promoda.com.mx. Promoda se reserva el derecho de cambiar este convenio y hacer cambios a cualquiera de los productos o servicios descritos en el sitio web www.promoda.com.mx en cualquier momento, sin ningún aviso o responsabilidad para él. Es responsabilidad de usted el verificar y consultar constantemente las presentes condiciones de uso a efecto de conocer dichos cambios o modificaciones. Promoda también se reserva el derecho, a su entera discreción, de negarle el acceso al Sitio Web en cualquier momento.</p>
      <p>
         A través de su Sitio Web, Promoda proporciona información acerca de sus productos y ofrece a sus usuarios registrados la posibilidad de su adquisición. Debido al contenido y la finalidad del Sitio Web, las personas que quieran adquirir dichos productos y beneficiarse de los descuentos y promociones deben registrarse, llenando el formulario de registro y siguiendo los pasos que Promoda les comunicará. Esto supone la adhesión por parte de la persona a los Términos y Condiciones en la versión publicada en el momento en que se acceda al Sitio Web.
      </p>
      <h3>Promoda no se hace responsable:</h3>
      <ol class="fq_points">
         <li>
            <p>
               De la utilización que los Usuarios puedan hacer de los materiales de este Sitio Web o páginas de enlace, ya sean prohibidos o permitidos, en infracción de los derechos de propiedad intelectual y/o industrial de contenidos del Sitio Web o de terceros.
            </p>
         </li>
         <li>
            <p>
               De los eventuales daños y perjuicios a los usuarios causados por un funcionamiento normal o anormal de las herramientas de búsqueda, de la organización o la localización de los contenidos y/o acceso al Sitio Web y, en general, de los errores o problemas que se generen en el desarrollo o instrumentación de los elementos técnicos que el Sitio Web o un programa facilite al Usuario.
            </p>
         </li>
         <li>
            <p>
               De los contenidos de aquellas páginas a las que usuarios puedan acceder desde enlaces incluidos en el Sitio Web, ya sean autorizados o no.
            </p>
         </li>
         <li>
            <p>
               Dar cumplimiento a obligaciones contraídas con nuestros clientes;
            </p>
            <p>
               Informar sobre cambios de nuestros productos o servicios;
            </p>
            <p>
               Evaluar la calidad del servicio, y
            </p>
         </li>
         <li>
            <p>
               Realizar estudios internos sobre hábitos de consumo
            </p>
         </li>
      </ol>
      <h3>Asimismo Promoda no será responsable en ningún caso cuando se produzcan:</h3>
      <ol class="fq_points">
         <li>
            <p>
               Errores o retrasos en el acceso al Sitio Web por parte del usuario a la hora de introducir sus datos en el formulario de pedido, la lentitud o imposibilidad de recepción por parte de los destinatarios de la confirmación del pedido o cualquier anomalía que pueda surgir cuando estas incidencias sean debidas a problemas en la red Internet, causas de caso fortuito o fuerza mayor y cualquier otra contingencia imprevisible ajena a la buena fe de Promoda.
            </p>
         </li>
         <li>
            <p>
               Puedes cambiar tu compra por cualquier otro producto del sitio o en los locales, de la misma colección&nbsp;ó puedes escoger por obtener una eCard por el importe de tu compra.
            </p>
         </li>
      </ol>
   






</div>



</div>

</div>






<?php include('./includes/footer.php');  ?> 