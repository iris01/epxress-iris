<?php include('./includes/header.php');  ?> 



<div class="row">
  
<div class="col-xs-12">   


<div class="bt_sections nf"> 
  

<h1>¿Preguntas Frecuentes?</h1> 

<hr>
      <h3>1. ¿Qué es Express?</h3> 
      
      <p class="nf">
         Express es una marca de moda con estilo bohemio y rocanrolero que nació en Buenos Aires, Argentina; tiene una identidad libre, aventurera y ultra femenina.
      </p>

      <h3>2. ¿Tienen tiendas físicas?</h3>
      <p class="nf">
         Si, consulta el local más cercano en
         <a href="/locales">http://www.express.com/locales</a>
      </p>
      
      <h3>3. ¿Cómo realizo una compra en Express.com?</h3>
      <p class="nf">Comprar en Express.COM es muy fácil y sencillo:</p>
      <ol class="fq_points">
         <li>
            <p class="nf">
               Navega nuestro Estore desde tu computadora, tablet o celular y elegí las prendas que querés comprar.
            </p>
         </li>
         <li>
            <p class="nf">
               Seleccioná color, talle y cantidad, y agregálas a tu carrito de compras.
            </p>
         </li>
         <li>
            <p class="nf">
               Registrate. Es necesario que estés registrado en nuestro sitio para poder avanzar con tu compra. Si ya tenes tu cuenta creada, solo tenes que ingresar tu dirección de correo electrónico y contraseña. Si sos nuevo en Express.com, creá tu cuenta
            </p>
         </li>
         <li>
            <p class="nf">
               Completá tu dirección de facturación y de envío.
            </p>
         </li>
         <li>
            <p class="nf">
               Elegí el método de envío más conveniente y selecciona el medio de pago.
            </p>
         </li>
         <li>
            <p class="nf">
               Realizá una última revisión del resumen de tu pedido y chequeá que toda la información sea correcta. Si queres hacer algún cambio, podes hacer click en “Editar Carrito”. Si estás conforme con el pedido, hace click en “Realizar Pago”.
            </p>
         </li>
      </ol>
      <p class="nf">
         Recibirás en tu casilla de correo electrónico un mail de confirmación con tu número de pedido. Si querés consultar el estado de tu orden y hacer un seguimiento del envío,
         <a title="Ir a Mi Cuenta" href="/customer/account">hacé click en Mi Cuenta</a> .
      </p>
      <p class="nf">
         Por cualquier consulta, escribinos a:
         <a href="mailto:contacto@Expressstore.com.ar">contacto@Expressstore.com.ar</a> o llamanos al: 3221-6869 de 9hs a 18hs.
      </p>
      <p class="nf">
         <a href="/paso-a-paso">
         Hace click acá para conocer más sobre Cómo Comprar
         </a>
      </p>
      <h3>
         4. ¿Cuáles son los métodos de envío?
      </h3>
      <ul class="f_child">  
         <li>
            <p class="nf"> <strong>Envío estándar:</strong> Los productos son entregados vía Correos Andreani, para todo el territorio nacional, de lunes a viernes entre las 9.00 y 18.00hs. El plazo de entrega estimado es dentro de los 10 días hábiles. Este plazo comienza a ser contabilizado una vez que el envío este en manos de Andreani.
            </p>
         </li>
         <li>
            <p class="nf"> <strong>Retiro en sucursal Andreani:</strong> Se podrá elegir la sucursal de Andreani de preferencia. El tiempo de entrega estimado es entre 48 y 72hs, y una vez que el pedido se encuentre disponible se enviará un notificación vía email avisando que está listo para ser retirado.
            </p>
            <p class="nf">
               El pedido estará disponible en la sucursal durante 7 días corridos y podrás consultar los horarios en:
               <a title="Ir a Andreani" href="http://www.andreani.com/Stores/Search" target="_blank">http://www.andreani.com/Stores/Search</a> .
            </p>
         </li>
         <li>
            <p class="nf"><strong>Pick up in store – Local Express:</strong> Se podrá retirar el pedido por: </p>
            <ol class="fq_points">
               <li>
                  <p class="nf">
                     Local DOT (Vedia 3626 - T: 011 5777-9738 – Horario: 10AM-10PM).
                  </p>
                  <ul class="f_child">
                     <li>
                        <p class="nf">
                           Si tu compra fue realizada a partir del día jueves a las 13hs hasta el día martes a las 12hs, tu pedido estará listo para ser retirado el próximo miércoles.
                        </p>
                     </li>
                     <li>
                        <p class="nf">
                           Si tu compra fue realizada desde el día martes a las 13 hs hasta el día jueves a las 12hs, tu pedido estará listo para ser retirado el próximo viernes
                        </p>
                     </li>
                  </ul>
               </li>
               <li>
                  <p class="nf">
                     Local Recoleta Mall (Vicente Lopez 2050,CABA - T: 011 4807-1858 – Horario: 10AM – 10AM)
                  </p>
                  <ul class="f_child">  
                     <li>
                        <p class="nf">
                           Si tu compra fue realizada a partir del día jueves a las 13hs hasta el día martes a las 12hs, tu pedido estará listo para ser retirado el próximo miércoles.
                        </p>
                     </li>
                     <li>
                        <p class="nf">
                           Si tu compra fue realizada desde el día martes a las 13 hs hasta el día jueves a las 12hs, tu pedido estará listo para ser retirado el próximo sábado
                        </p>
                     </li>
                  </ul>
               </li>
            </ol>
            <p class="nf">Te enviaremos un email avisándote que tu pedido está listo para ser retirado.</p>
         </li>
      </ul>
      <p class="nf">
         Recordá que el retiro del pedido está a cargo del titular o de un tercero con autorización firmada por el titular y documento que acredite su identidad.
      </p>
      <h3>5. ¿Cuál es el costo de entrega?</h3>
      <p class="nf">
         El valor de la entrega será indicado durante su compra, antes de que el pedido sea finalizado. Este costo corre por cuenta de nuestros clientes, excepto en los casos en que Express esté haciendo una acción promocional.
      </p>
      <h3>6. ¿Cuáles son los medios de pago?</h3>
      <p class="nf">
         En &nbsp;stevemadden.COM se puede realizar el pago por medio de &nbsp;tarjeta de crédito y cupón de pago a través de Mercado Pago. El sistema de cobranza a través de tarjeta de crédito es 100% seguro. Los números son encriptados y no sufren ningún tipo de amenaza en nuestro banco de datos, siendo enviados directamente a la administradora de la tarjeta de crédito.
      </p>
      <h3>7. ¿Puedo tener seguimiento de mi pedido?</h3>
      <p class="nf">
         Si, en cuanto tu pedido se envíe, recibirás una confirmación por correo electrónico con un código de seguimiento. El mismo podes rastrearlo ingresando en el sitio de Andreani.
      </p>
      <p class="nf">
         También puedes verificar el estado del pedido iniciando sesión en tu cuenta en nuestro sitio web.
      </p>
      <p class="nf">
         Si por algún motivo no recibes el código de seguimiento revisa tu carpeta de correo no deseado o puedes enviar tu consulta a
         <a href="mailto:contacto@Expressstore.com.ar">contacto@Expressstore.com.ar</a> y llamarnos al: 3221-6869 de 9hs a 18hs o consultarnos vía chat en nuestro sitio.
      </p>
      <h3>
         8. ¿Cuáles son los posibles estados de mi pedido?
      </h3>
      <p class="nf">
         Tu pedido puede aparecer en alguno de los siguientes estados:
      </p>
      <ol class="fq_points">
         <li>
            <p class="nf">
               “En Búsqueda”: quiere decir que recibimos tu pedido con éxito y está siendo procesado en nuestros depósitos.
            </p>
         </li>
         <li>
            <p class="nf">
               “En depósito M”: significa que se encuentra en estado de facturación.
            </p>
         </li>
         <li>
            <p class="nf">
               “Con orden en depósito M”: tu pedido ya fue preparado y facturado y está esperando ser retirado por el correo.
            </p>
         </li>
         <li>
            <p class="nf">
               “Despachado”: tu pedido ya salió de nuestro depósito. Recibirás una confirmación por correo electrónico con un código de seguimiento para utilizarlo ingresando en el sitio de Andreani.
            </p>
         </li>
      </ol>
      <h3>9. ¿Clientas Exclusive?</h3>
      <p class="nf">
         El descuento para clientas exclusive lo podés utilizar tanto en nuestros locales como en el sitio Express.com
      </p>
      <h3>10. ¿Puedo cambiar o devolver un producto?</h3>
      <p class="nf">Sí, tienes 3 opciones:</p>
      <ol class="fq_points">
         <li>
            <p class="nf">
               En nuestros locales, de acuerdo a la
               <a title="Ver Política de Cambios y Devoluciones" href="/cambios">Política de Cambios y Devoluciones</a> .
            </p>
         </li>
         <li>
            <p class="nf">Desde tu domicilio:</p>
            <ul class="f_child">
               <li>
                  <p class="nf">
                     Podrás solicitar que el correo pase a retirar tu producto en tu domicilio, y gestionar ya sea el cambio por una nueva prenda o la devolución de tu dinero.
                  </p>
               </li>
            </ul>
         </li>
         <li>
            <p class="nf">
               eCard: puedes escoger por obtener una eCard por el valor del producto abonado. Podrá ser utilizada únicamente para realizar otra compra online y el envío será bonificado. Tienes 6 meses de vigencia para poder utilizarla.
            </p>
         </li>
      </ol>
      <p class="nf">
         Recordá que las prendas Vintage pueden ser cambiadas únicamente en nuestros locales Vintage o desde tu domicilio, solicitando que el correo retire la prenda.
      </p>
      <p class="nf">
         Puedes enviar un mail a
         <a href="mailto:contacto@Expressstore.com.ar">contacto@Expressstore.com.ar</a> detallando tu decisión.
      </p>
      <p class="nf">
         <a href="/cambios/">
         Hace click acá para conocer más sobre la Política de Cambios y Devoluciones
         </a>
      </p>
   
</div>



</div>

</div>






<?php include('./includes/footer.php');  ?> 