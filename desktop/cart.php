<?php include('./includes/header.php');  ?> 




<div class="row">


   <div class="col-xs-7">
      <div class="col-xs-12 resume_cart">
         
         <h1>Shopping Bag <span> (3) </span></h1>


         <div class="row guide_titles">
            <div class="col-xs-8">
               <h4>ITEM</h4>
            </div>
            <div class="col-xs-2">
               <h4>QTY</h4>
            </div>
            <div class="col-xs-2">
               <h4>TOTAL</h4>
            </div>
         </div>
         <div class="row showp">
            <!-- producto --> 
            <div class="col-xs-8">
               <div class="row">
                  <div class="col-xs-4"> 
                     <img class="img-responsive" src="./images/catalog/f_2.jpg" alt="">
                  </div>
                  <div class="col-xs-8">
                     <div class="r_title">
                        <h2>Soft Twill Tunic Dress</h2>
                        <ul class="more_info">
                           <li>
                              <p>Size: <span class="cblack"> X Small </span></p>
                           </li>
                           <li>
                              <p>Color: <span class="cblack"> MULTI 29 </span></p>
                           </li>
                           <li><strike>$42.40</strike><span class="cred"> $23.00 </span>       </li>
                           <li><small>Now 30% Off!</small>                      </li>
                           <li><small>Price Reflects Dscount</small>                    </li>
                           <li> <i>"My Favorite Sundress" - Marina</i>                    </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <ul class="edit_actions">
                  <li class="n_m"><a href="#">Edit</a></li>
                  <li><a href="#">Remove</a></li>
                  <li><a href="#">Save for Later</a></li>
               </ul>
            </div>
            <div class="col-xs-2">
               <select name="#" id="">
                  <option>QTY: 1</option>
                  <option>QTY: 1</option>
                  <option>QTY: 1</option>
                  <option>QTY: 1</option>
               </select>
            </div>
            <div class="col-xs-2"> <strong class="ex-bold"> 1392.00 </strong> </div>
         </div>
         <!-- /producto --> 
         <div class="row showp">
            <!-- producto --> 
            <div class="col-xs-8">
               <div class="row">
                  <div class="col-xs-4"> 
                     <img class="img-responsive" src="./images/catalog/9.jpg" alt="">
                  </div>
                  <div class="col-xs-8">
                     <div class="r_title">
                        <h2>Floral Print</h2>
                        <ul class="more_info">
                           <li>
                              <p>Size: <span class="cblack"> X Small </span></p>
                           </li>
                           <li>
                              <p>Color: <span class="cblack"> MULTI 29 </span></p>
                           </li>
                           <li><strike>$42.40</strike><span class="cred"> $23.00 </span>       </li>
                           <li><small>Now 30% Off!</small>                      </li>
                           <li><small>Price Reflects Dscount</small>                    </li>
                           <li> <i>"My Favorite Sundress" - Marina</i>                    </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <ul class="edit_actions">
                  <li class="n_m"><a href="#">Edit</a></li>
                  <li><a href="#">Remove</a></li>
                  <li><a href="#">Save for Later</a></li>
               </ul>
            </div>
            <div class="col-xs-2">
               <select name="#" id="">
                  <option>QTY: 1</option>
                  <option>QTY: 1</option>
                  <option>QTY: 1</option>
                  <option>QTY: 1</option>
               </select>
            </div>
            <div class="col-xs-2"> <strong class="ex-bold"> 1392.00 </strong> </div>
         </div>
         <!-- /producto --> 
      </div>
   </div>


   <div class="col-xs-5">
      <div class="bag_summary">
         <h2>BAG SUMMARY </h2>
         <div class="info_price">  
            <i>Spend only <span>$33.17</span> MORE TO GET <span> FREE SHIPPING</span> </i>
         </div>
         <div class="row sub_price">
            <div class="col-xs-8">
               <p>Merchandise Subtotal:</p>
            </div>
            <div class="col-xs-4">
               <p class="text-right"> 1.322 MXN </p>
            </div>
         </div>
         <div class="row shipping_price">
            <div class="col-xs-8">
               <p>Shipping: Standar (Get it April 12-16) <span><a href="#">Change</a></span></p>
            </div>
            <div class="col-xs-4">
               <p class="text-right"> 420.00 MXN </p>
            </div>
         </div>
         <div class="row">
            <div class="total_price">
               <div class="col-xs-6">
                  <p>Total Before Tax</p>
               </div>
               <div class="col-xs-6">
                  <p class="text-right informative_price">720.00 MXN</p>
               </div>
            </div>
         </div>
         <button type="submit" class="btn btn-express cart w10 btn-default">CHECKOUT</button> 




<div class="one_last">
   
<h3>One Last Look</h3>

 

<div class="row">  

<div class="col-xs-4">      
<img class="img-responsive" src="./images/cart/black.jpg" alt=""> 
<div class="also_des">
<p>Express One Eleven Deep V-Neck Choker</p>
<p class="also_price">498 MXN</p>
</div>
</div>


<div class="col-xs-4">     
<img class="img-responsive" src="./images/cart/gray.jpg" alt=""> 
<div class="also_des">
<p>Deep V Turtleneck Ribbed Sleeveless</p>
<p class="also_price">798 MXN</p>
</div>
</div>



<div class="col-xs-4">     
<img class="img-responsive" src="./images/cart/jean.jpg" alt=""> 
<div class="also_des">
<p>Express One Eleven Strappy Front Bodysuit</p>
<p class="also_price">498 MXN</p>
</div>
</div>

</div>




</div>


</div>


</div> <!-- col-xs-5 --> 

</div>





</div> <!-- container -->



 
 
<div class="color_options">  

<div class="row">   

<ul class="actions_cart">  

<li id="security"> 
<p class="footer-utility-box">
<span class="footer-utility-title">Secure Transactions</span>
<span class="footer-utility-info">All transactions are safe and secure.</span>
</p>
</li>

<li id="returns">
<p class="footer-utility-box">
<span class="footer-utility-title">Simple Returns</span>
<span class="footer-utility-info">In store or online. Up to 90 days.</span>
</p> 
</li>

<li id="support">
<p class="footer-utility-box">
<span class="footer-utility-title">Available 24/7</span>
<span class="footer-utility-info">Call us at <a class="show-for-touch" href="tel:+18883971980">(888) 397-1980</a></span>
</p>
</li>

</ul>

</div>

</div>

 




<!-- cierra en pie de pagina --> 




<?php include('./includes/footer.php');  ?>