<?php include('./includes/header.php');  ?> 

</div>



<div class="fondo_cuenta"> 

<div class="container">

<div class="row">
  
<div class="col-xs-12"><h1 class="my_account">Mi cuenta</h1></div>

</div>



<div class="row">
  
<div class="sidebar"> 

<div class="col-xs-3 sr">
  
<ul class="dotted_line">
   
   <li class="selected">  <a href="#"><span class="mis_pedidos icon"></span> <span>Mis pedidos</span>  </a> </li>
   <li>  <a href="#"><span class="mis_devoluciones icon"></span> <span>Mis devoluciones</span></a>      </li>
   <li>  <a href="#"><span class="mis_cupones icon"></span> <span> Mis cupones </span> </a>  </li>
   <li>  <a href="#"><span class="mi_informacion icon"></span> <span> Información de cuenta </span> </a>  </li>
   <li>  <a href="#"><span class="mis_direcciones icon"></span> <span> Libreta de direcciones </span> </a>  </li>
   <li>  <a href="#"><span class="mis_tarjetas icon"></span> <span> Mis Tarjetas </span> </a>  </li>
   <li>  <a href="#"><span class="mis_favoritos icon"></span> <span> Favoritos </span> </a>  </li>
   <li>  <a href="#"><span class="mis_noti icon"></span> <span> Notificaciones via Email? </span> </a>  </li>

</ul>  

</div>

</div>


<div class="col-xs-9 sl">






<div class="user_panel">
    
  <h2>MIS PEDIDOS</h2>
  <p class="t_standar"> Usted no ha realizado Pedidos </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-3"> FECHA </div>
      <div class="col-xs-3"> PEDIDO # </div>
      <div class="col-xs-3"> DIRECCIÓN </div>
      <div class="col-xs-3 text-right"> TOTAL PRICE </div>
   </div>
 


   <!-- Producto Agregado --> 

   <div class="row rl"> 

      <div class="col-xs-3 data_general f_auto"> 
         <h3>12/03/2016</h3>
      </div>

      <div class="col-xs-3 data_general f_auto">
          <h3>399S03S9VV</h3>
      </div>

      <div class="col-xs-3 f_auto">
         <p class="normal_18 description">Calle Altitud #222</p>
         <p class="normal_18 description">Colonia Polanco</p>
      </div>

      

      <div class="col-xs-3 data_general f_auto">
         <h3 class="text-right">$98.00</h3>
      </div>

   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>











<div class="user_panel">
    
  <h2>MIS DEVOLUCIONES</h2>
  <p class="t_standar"> Usted no ha realizado Devoluciones </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-2"> PEDIDO # </div>
      <div class="col-xs-2"> SKU </div>
      <div class="col-xs-3"> FECHA DE DEVOLUCIÓN </div>
      <div class="col-xs-3"> MOTIVO </div>
      <div class="col-xs-2 text-right"> CODIGO RASTREO </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-2 data_general f_auto"> 
         <h3> 740 </h3>
      </div> 

      <div class="col-xs-2 data_general f_auto">
          <h3>XYZN3S9202</h3>
      </div>

      <div class="col-xs-3 data_general f_auto">
          <h3>15/10/2016</h3>
      </div>

      <div class="col-xs-3 f_auto">
         <p class="normal_18 description"> La talla no era la adecuada</p>
      </div>

      

      <div class="col-xs-2 data_general f_auto">
         <h3 class="text-right">732839282812392</h3>
      </div>

   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>














<div class="user_panel">
    
  <h2>MIS CUPONES</h2>
  <p class="t_standar"> Usted no tiene cupones </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-6"> CLAVE CUPON # </div>
      <div class="col-xs-6"> FECHA DE EXPIRACIÓN </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-6 data_general f_auto"> 
         <h3> JULIO02DESCUENTO </h3>
      </div> 

      <div class="col-xs-6 data_general f_auto">
          <h3>31/07/2016</h3>
      </div>


   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>









 




<div class="user_panel">
    

  <h2>INFORMACIÓN DE LA CUENTA</h2>


<div class="row">

     <div class="col-xs-12"> 
  
  <ul class="panel_info">
   <li> <p>SEXO: <span> Masculino  </span> </p></li>     
   <li> <p>E-MAIL: <span> ernesto@pictures.com  </span> </p></li>     
   <li> <p>NOMBRE: <span> Ernesto Cárdenas  </span> </p></li>   
   <li> <p>FECHA DE NACIMIENTO: <span> 15/10/1990  </span> </p></li> 
  </ul>

     </div>

 </div>


</div>









<div class="user_panel">
  
  <h2> Libreta de Direcciones</h2>
  <p class="t_standar"></p>
  <a id="new-address" class="link-account">Agregar nueva dirección</a>
  <h4 class="tl">AGREGAR NUEVA DIRECCION</h4>

<div id="container-address" class="row">
<hr class="divider">

<form role="form" action="#" class="form_comun">



            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="Nombre">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="Apellido">
</div>

            </div>




            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="Teléfono">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="Teléfono Adicional">
</div>

            </div>



            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="Código Postal">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="Calle">
</div>

            </div>



            <div class="form-group">
 
<div class="col-xs-6 pf">
    <input type="text" class="form-control w10" id="#" placeholder="No. Exterior">      
</div>

<div class="col-xs-6 fp"> 
    <input type="text" class="form-control w10" id="#" placeholder="No. Interior">
</div>

            </div>




            <div class="form-group">
               <select class="gender">
                  <option value="">Colonia *</option>
                  <option value="male">ESTRELLA</option>
                  <option value="female">1</option>
               </select>
            </div>





            <div class="form-group">
 
<div class="col-xs-6 pf">
               <select class="date w10">
<option value="" selected="selected">Estado *</option>
<option value="01">1</option>
<option value="02">2</option>
<option value="03">3</option>
<option value="04">4</option>
<option value="05">5</option>
<option value="06">6</option>
<option value="07">7</option>
<option value="08">8</option>
               </select>
</div>

<div class="col-xs-6 fp"> 
               <select class="date w10">
<option value="" selected="selected">Ciudad *</option>
<option value="01">ENE</option>
<option value="02">FEB</option>
<option value="03">MAR</option>
<option value="04">ABR</option>

               </select>
</div>



            </div>


<p>* Campos Obligatorios</p>

            <button type="submit" class="btn btn-express register btn-default">GUARDAR</button>
            <a id="link-cancel">Cancelar</a>

         </form>


</div>
<div class="row">
  <div class="info-address">
      <div class="col-xs-12">
        <div class="header-address">
          <h4>Dirección por defecto</h4>
        </div>
        <div class="container-address">
          <input type="radio" name="address">
          <ul class="list-left">
                <li>Iris Cuevas</li>
                <li>Coatl No. 41</li>
                <li>Ciudad de Mexico,MX 04508</li>
                <li>MX</li>
                <li>(734)134-7503</li>
          </ul>
          <div class="action-links">
            <ul>
              <li><a id="link-edit">Editar</a></li>
              <li class="last"><a href="#">Eliminar</a></li>
            </ul>
          </div>
        </div>
      </div>
  </div>
</div>

</div>









<div class="user_panel">
    
  <h2>MIS TARJETAS</h2>
  <p class="t_standar"> Usted no tiene tarjetas </p>


<div class="resume_cart">


   <div class="row cart_detail rl"> 
      <div class="col-xs-4"> TARJETA </div>
      <div class="col-xs-4"> TERMINACIÓN </div>
      <div class="col-xs-4"> COMPRAS </div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl"> 

      <div class="col-xs-4 data_general f_auto"> 
         <h3> VISA </h3>
      </div> 

      <div class="col-xs-4 data_general f_auto">
          <h3>... .... 232</h3>
      </div>


      <div class="col-xs-4 data_general f_auto">
          <h3>05</h3>
      </div>


   </div>
   <!-- Producto Agregado -->



   <div class="bottom_margin_cart"> </div>


 </div>

</div>

<div class="user_panel">
    
  <h2>FAVORITOS</h2>
  <div class="row">
  <form action="#" role="form" class="form_comun">
    <div class="form-group">
      <div class="col-xs-4 pf">
        <label>Buscar una lista de deseos</label>
      </div>
      <div class="col-xs-4 fp">
        <input type="text" class="form-control w10" id="#">
      </div>
      <div class="col-xs-3 fp">
        <button type="submit" class="btn-favorite">Buscar</button>
      </div>
    </div>
  </form>
</div>
  <p class="t_standar">No tienes listas de deseos.</p>

  <div class="resume_cart">
   <div class="row cart_detail rl"> 
      <div class="col-xs-4"><p>PRODUCTO</p></div>
      <div class="col-xs-2"> FECHA </div>
      <div class="col-xs-2"><p>DISPONIBILIDAD</p></div>
      <div class="col-xs-2"> PRECIO </div>
      <div class="col-xs-2 text-right"></div>
   </div>
 


   <!-- Producto Agregado -->
   <div class="row rl">
      <div class="col-xs-2 data_general f_auto">
         <img class="img-responsive" src="./images/catalog/1.jpg" alt="">
      </div>
      <div class="col-xs-2 data_general f_auto p_auto"> 
         <ul class="detail-product">
           <li>Blusa Negra</li>
           <li><p>ID del articulo:
                  AN171AT52RUDDFMX-570335</p></li>
           <li>Talla: M</li>
           <li><a href="#">Eliminar producto</a></li>
         </ul>
      </div>
       <div class="col-xs-2 data_general f_auto">
          <p>15/10/2016</p>
      </div>

      <div class="col-xs-2 data_general f_auto">
          <p>Out of stock</p>
      </div>

      <div class="col-xs-2 f_auto">
         <p class="normal_18 description"> 923 MXN</p>
      </div>
      <div class="col-xs-2 f_auto">
        <button class="btn-tbl">Agregar a carrito</button>
      </div>     
   </div>
   <!-- Producto Agregado -->

   <div class="bottom_margin_cart"> </div>


 </div>

</div>





















<div class="user_panel">
    
  <h2>ADMINISTRAR NEWSLETTER</h2> 


<div class="row"> 

     <div class="col-xs-12"> 
     
<div class="select_news">

                <form action="#">  

                  <input style="margin-right: 5px;" id="nb1" value="nb1" name="nb1" type="checkbox">
                  <label for="nb1" class="#">Newsletter</label>
                <br> 
                  <input style="margin-right: 5px;" id="nb2" value="nb2" name="nb2" type="checkbox">
                  <label for="nb2" class="#">Notificaciones De  Marketing</label>
              
              <hr>

 <button type="submit" class="btn btn-express btn-default">ENVÍAR</button>

                </form>

</div>

     </div>

 </div>

</div>














</div> <!-- col 9 --> 




</div>

</div>


</div>



<?php include('./includes/footer.php');  ?>  